//  == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == =
//  Tracker with SIM800L
//
//  Get location via SIM800L over GSM, using Triangulation.
//
//  This code is written by Aldi Andika Pratama, 2019.
//  == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == == =


//#include <LowPower.h>
#include <SoftwareSerial.h>


SoftwareSerial mySerial(7, 8); //rx,tx

// STB Information
String deviceID = "1";
String mac = "352178070644123";
String serialNumber = "gsm001";
String flagDevice = "1";

bool isGPRSInit = false;
int FLAG = 0;


String StringLocation = "";
String subLon = "";
String subLat = "";
String subDate = "";
String RawLoc = "";
String Responses = "";

int countInit = 0;

void rstProsedur(int pinReset) {
  pinMode(pinReset, OUTPUT);
  digitalWrite(pinReset, HIGH);
  delay(500);
  digitalWrite(pinReset, LOW);
  delay(106);
  digitalWrite(pinReset, HIGH);
}

//Function to communicate with SIM800L module
String SIM800_send(String incoming) {
  mySerial.println(incoming); delay(500); //Print what is being sent to GSM module
  String result = "";
  while (mySerial.available()) //Wait for result
  {
    char letter = mySerial.read();
    result = result + String(letter); //combine char to string to get result
  }
  return result; //return the result
}

//Function to Initiate SIM800L module
void Init() {
  Responses = SIM800_send("AT+CGATT=1");
  Serial.println(Responses);
  delay(1000);

  Responses = SIM800_send("AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"");
  Serial.println(Responses);
  delay(1000);

  Responses = SIM800_send("AT+SAPBR=3,1,\"APN\",\"INTERNET\"");
  Serial.println(Responses);
  delay(2000);

  Responses = SIM800_send("AT+SAPBR=1,1");
  Serial.println(Responses);
  delay(2000);

  Responses = SIM800_send("AT+SAPBR=2,1");
  Serial.println(Responses);
  delay(2000);

  Responses = SIM800_send("AT+CLBS=1,1");
  Serial.println(Responses);
  delay(1000);

  isGPRSInit = true;
}

//Funtion to terminate GPRS mode
void gprsterm() {
  Responses = SIM800_send("AT+SAPBR=0,1");
  Serial.println(Responses);
  delay(1000);
}

//Funvtion to parse raw location
String getGPSParse() {
  //  Serial.println("GETTING GPS LOCATION");
  String loc = "";
  String locParsed = "";

  Responses = SIM800_send("AT+CLBS=1,1");
  //  Serial.println(Responses);
  loc = Responses;

  return loc;
}

void parser(String data) {
  int dataLen = data.length();

  String subData = "";
  int subDataLen = 0;
  int count = 0;
  int lim[5];

  // Get sub data from serial output
  subData = data.substring(14);
  subDataLen = subData.length();

  for (int i = 0; i < subDataLen; i++) {
    if (subData.charAt(i) == ',') {
      lim[count] = i;
      count++;
    }
  }

  subLon = subData.substring(lim[0] + 1, lim[1]);
  subLat = subData.substring(lim[1] + 1, lim[2]);

  Serial.print("Longitude = "); Serial.println(subLon);
  Serial.print("Latitude = "); Serial.println(subLat);

  // FLAG_SEND = true;
}

void sendToServer() {
  //  if(FLAG_SEND){
  Responses = SIM800_send("AT+HTTPINIT");
  Serial.println(Responses);
  delay(1000);

  Responses = SIM800_send("AT+HTTPPARA=\"CID\",1");
  Serial.println(Responses);
  delay(1000);

  Responses = SIM800_send("AT+HTTPPARA=\"URL\",\"http://assettracking.online/store_gsm/" + deviceID + "/" + mac + "/" + serialNumber + "/" + subLat + "/" + subLon + "/" + flagDevice + "\"");
  Serial.println(Responses);
  delay(2000);

  Responses = SIM800_send("AT+HTTPSSL=0");
  Serial.println(Responses);
  delay(1000);

  Responses = SIM800_send("AT+HTTPACTION=0");
  Serial.println(Responses);
  delay(1000);

  //    FLAG_SEND = false;
  //    FLAG_PARSING = false;
  //  }
}

void gotoSleep() {
  Responses = SIM800_send("AT+CFUN=0");
  Serial.println(Responses);
  delay(1000);
}

void wakeUp() {
  Responses = SIM800_send("AT+CFUN=1");
  Serial.println(Responses);
  delay(1000);
}

void setup() {
  mySerial.begin(9600);
  Serial.begin(9600);

  //  Init();

}

void loop() {
  Responses = SIM800_send("AT+SAPBR=3,1,\"APN\",\"INTERNET\"");
  Serial.println(Responses);
  delay(2000);

  Responses = SIM800_send("AT+SAPBR=1,1");
  Serial.println(Responses);
  delay(2000);

  Responses = SIM800_send("AT+SAPBR=2,1");
  Serial.println(Responses);
  delay(2000);

  RawLoc = getGPSParse();
  Serial.println(RawLoc);

  parser(RawLoc);

  if ((subLat.length() <= 3) || (subLon.length() <= 3))  {
    Serial.println("Error Location");
    delay(3000);
  } else {
    Serial.println("Kirim Data");
    sendToServer();
    delay(1200000);
  }

  

  //  countInit++;
  //
  //    Serial.println(countInit);
  //

  //  switch (FLAG) {
  //    case 0:
  //      //      wakeUp();
  //      StringLocation = getGPSParse();
  //      delay(1000);
  //      FLAG = 1;
  //      break;
  //    case 1:
  //      parser(StringLocation);
  //      delay(500);
  //      FLAG = 2;
  //      break;
  //    case 2:
  //      sendToServer();
  //      //      gotoSleep();
  //      //      LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
  //      delay(1000);
  //      FLAG = 0;
  //      break;
  //  }

}

//  if(FLAG_PARSING){
//    parser(StringLocation);
////    delay(500);
////    sendToServer();
////    Serial.println(lokasi.length());
////    FLAG_PARSING = false;
//  }
//  delay(500);
